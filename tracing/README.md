    helm repo add jetstack https://charts.jetstack.io
    helm repo add jaegertracing https://jaegertracing.github.io/helm-charts
    helm repo update
    kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.13.1/cert-manager.crds.yaml
    helm install \
      cert-manager jetstack/cert-manager \
      --namespace cert-manager \
      --create-namespace \
      --version v1.13.1
    kubectl create ns observability
    kubectl apply -f - <<EOF
    kind: RoleBinding
    apiVersion: rbac.authorization.k8s.io/v1
    metadata:
      name: jaeger-operator-in-myproject
      namespace: observability
    subjects:
    - kind: ServiceAccount
      name: jaeger-operator
      namespace: observability
    roleRef:
      kind: Role
      name: jaeger-operator
      apiGroup: rbac.authorization.k8s.io
    EOF
    
    kubectl apply -f https://github.com/jaegertracing/jaeger-operator/releases/download/v1.49.0/jaeger-operator.yaml -n observability
    kubectl apply -f - <<EOF
    apiVersion: jaegertracing.io/v1
    kind: Jaeger
    metadata:
        name: simplest
    EOF
