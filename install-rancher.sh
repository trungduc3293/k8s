#! bin/bash
helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
kubectl create namespace cattle-system
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.11.0/cert-manager.crds.yaml
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.11.0
helm install rancher rancher-stable/rancher --namespace cattle-system --set hostname=rancher.demo.vn --set global.cattle.psp.enabled=false --set bootstrapPassword=admin
kubectl -n cattle-system create secret tls rancher-cert-secret --cert=demo.pem --key=demo.pkcs8
kubectl delete ingress rancher -n cattle-system
kubectl apply -f ingress-nginx/rancher.yaml
